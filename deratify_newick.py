#This script removes the rates from generated newick trees, since not all applications like them.
import os
import re

n_d = "newick_trees/"
c_n_d = "ignore/clean_newick_trees/"
for filename in os.listdir(n_d):
    if ".newick" not in filename: continue
    tree = open(n_d + filename, "r").read()
    new_tree = re.sub(r"\[.*?\]", "", tree)
    f = open(c_n_d + filename, "w")
    f.write(new_tree)
    f.close()