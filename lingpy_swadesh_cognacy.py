#This script uses the automatic cognacy determination functionality of lingpy to annotate the Swadesh lists.
from lingpy import *
import csv
import os
dialects = {}
cldf_dir = "/Users/florianm/Dropbox/Uni/Research/LiMiTS/tools/cariban_cldf/raw/"

with open(cldf_dir + "cariban_language_list.csv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=",")
    for row in csv_reader:
        if row["Dialect_Of"] != "":
            dialects[row["ID"]] = row["Dialect_Of"]
            
reader = csv.DictReader(open(cldf_dir + "cariban_swadesh_list.csv"))
out = [["ID","CONCEPT","IPA","DOCULECT"]]
for i, row in enumerate(reader):
    lg_id = row["Language_ID"]
    if row["Value"] == "" or row["Language_ID"] == "tam": continue
    if lg_id in dialects.keys():
        lg_id = dialects[lg_id]
    out.append([
        i+1,
        row["Feature_ID"],
        row["Value"].split("; ")[0].replace("-",""),
        lg_id,
    ])
with open("lingpy_temp.tsv","w") as f:
    writer = csv.writer(f,delimiter="\t")
    for row in out:
        writer.writerow(row)
lex = LexStat("lingpy_temp.tsv")
os.remove("lingpy_temp.tsv")

lex.get_scorer()
lex.cluster(
    method="lexstat",
    threshold=0.6,
    ref="cognates"
)

lex.output(
    "tsv",
    filename="lingpy_output",
    ignore="all",
    prettify=False
)
cognacy_judgments = [["Language_ID","Feature_ID","Value"]]
language_values = {}
with open("lingpy_output.tsv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter="\t")
    for row in csv_reader:
        if row["CONCEPT"] == None: continue
        cognacy_judgments.append([
            row["DOCULECT"],
            row["CONCEPT"],
            row["COGNATES"]
        ])
        if row["COGNATES"] not in language_values.keys():
            language_values[row["COGNATES"]] = {}
        language_values[row["COGNATES"]][row["DOCULECT"]] = row["IPA"]
with open("./feature_files/lingpy_swadesh.csv","w") as f:
    writer = csv.writer(f,delimiter=",")
    for row in cognacy_judgments:
        writer.writerow(row)

os.remove("lingpy_output.tsv")