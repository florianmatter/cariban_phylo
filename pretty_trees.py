from Bio import Phylo
from Bio.Phylo.PhyloXML import Phylogeny
import csv
import copy
import unidecode
import os
from matplotlib import pyplot as plt

lg_dic = {}
with open("/Users/florianm/Dropbox/Uni/Research/LiMiTS/tools/cariban_cldf/raw/cariban_language_list.csv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=",")
    for row in csv_reader:
        if row["Sampled"] == "y" or row["ID"] == "tam":
            lg_dic[row["ID"]] = unidecode.unidecode(row["Orthographic"].replace(" ", "_")).replace("'", "")

def rename_terminals(tree):
    for node in tree.get_terminals():
        node.name = lg_dic[node.name]

def remove_axes():
    plt.gca().get_yaxis().set_visible(False)
    plt.gca().spines["top"].set_visible(False)
    plt.gca().spines["left"].set_visible(False)
    plt.gca().spines["right"].set_visible(False)

def color_branches(tree):
    tree.root.color = "#cccccc"
    branches = {
        "Parukotoan": ["kax", "hix", "#9900ff"],
        "Taranoan": ["tri", "car", "#ff3300"],
        "Guianan": ["way", "car", "#ff9933"],
        "Pekodian": ["bak", "ikp", "#00cc00"],
        "": ["yab", "map", "#00ffff"],
        "Pemongan": ["mac", "pem", "kap", "#0033cc"],
        "Pemon-Panare": ["pem", "pan", "#33ccff"],
        "Venezuelan": ["pan", "yab", "#0099cc"]
    }
    for label, items in branches.items():
        targets = []
        for target in items[0:-1]:
            targets.append({"name": target})
        try:
            clade = tree.common_ancestor(*targets)
        except TypeError:
            print("No common ancestor of %s" % ", ".join(items[0:-1]))
        except ValueError:
            print("%s not found in tree" % ", ".join(items[0:-1]))
        if clade.root != tree.root:
            clade.color = items[-1]
            clade.name = label
                    
newick_dir = "./newick_trees/"
for filename in os.listdir(newick_dir):
    if "newick" not in filename: continue
    tree_name = filename.replace(".newick", "") + "_tree"
    phylo_tree = Phylo.read(newick_dir + filename, "newick")
    phylo_tree.root.branch_length = 0
    
    phylo_tree_colored = copy.deepcopy(phylo_tree)
    # if "_80" not in filename: continue
    #     phylo_tree_80 = copy.deepcopy(phylo_tree)
    #     phylo_tree_colored_80 = copy.deepcopy(phylo_tree)


    rename_terminals(phylo_tree)
    phylo_tree.ladderize(reverse=True)
    Phylo.draw(
        phylo_tree,
        do_show=False
    )
    remove_axes()
    plt.savefig("images/%s_raw.pdf" % tree_name)
    
    # if "_80" not in filename:
    #     phylo_tree_80.collapse_all(lambda c: c.confidence < 0.8)
    #     phylo_tree_80.ladderize(reverse=True)
    #     rename_terminals(phylo_tree_80)
    #     Phylo.draw(
    #         phylo_tree_80,
    #         do_show=False
    #     )
    #     remove_axes()
    #     plt.savefig("images/%s_80.png" % tree_name)
        
    
    phylo_tree_colored.ladderize(reverse=True)
    color_branches(phylo_tree_colored) 
    rename_terminals(phylo_tree_colored)
    Phylo.draw(
        phylo_tree_colored,
        do_show=False,
        show_confidence=False,
    )
    remove_axes()
    plt.savefig("images/%s_colored.pdf" % tree_name)

    # if "_80" not in filename:
  #       phylo_tree_colored_80.collapse_all(lambda c: c.confidence < 0.8)
  #       phylo_tree_colored_80.ladderize(reverse=True)
  #       color_branches(phylo_tree_colored_80)
  #       rename_terminals(phylo_tree_colored_80)
  #       Phylo.draw(
  #           phylo_tree_colored_80,
  #           do_show=False,
  #           show_confidence=False,
  #       )
  #       remove_axes()
  #       plt.savefig("images/%s_colored_80.png" % tree_name)