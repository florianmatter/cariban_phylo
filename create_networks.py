#This script uses SplitsTree to create NeighborNet analyses of the distance matrices
import subprocess
import os
import csv
import unidecode
import sys
from cariban_helpers import *

lg_dic = {}
for lg in lg_order():
    lg_dic[lg] = unidecode.unidecode(get_name(lg).replace(" ", "_")).replace("'", "")

glotto_dic = {}
for lg in lg_order("glottocode"):
    glotto_dic[lg] = unidecode.unidecode(get_name(lg).replace(" ", "_")).replace("'", "")

matrix_dir = "./distance_matrices/"
for filename in os.listdir(matrix_dir):
    if ".nex" not in filename: continue
    text = open(matrix_dir + filename).read()
    for i, j in lg_dic.items():
        if j not in text:
            print("Replacing %s with %s" % (i, j))
            text = text.replace("\n%s  " % i, "\n%s  " % j)
            text = text.replace("'%s'" % i, "'%s'" % j)
    for i, j in glotto_dic.items():
        if j not in text:
            print("Replacing %s with %s" % (i, j))
            text = text.replace("\n%s  " % i, "\n%s  " % j)
            text = text.replace("'%s'" % i, "'%s'" % j)
    with open((matrix_dir + filename), 'w') as f:
        f.write(text)

    splitstree_cmd = """begin SplitsTree;
    EXPORT FILE=test.phy.nex FORMAT=nexus REPLACE=yes;
    IMPORT FILE=/Users/florianm/Dropbox/Uni/Research/LiMiTS/trees/cariban_phylo/distance_matrices/%s;
    UPDATE;
    EXPORTGRAPHICS format=EPS file=/Users/florianm/Dropbox/Uni/Research/LiMiTS/trees/cariban_phylo/images/%s.eps REPLACE=yes;
    EXPORTGRAPHICS format=SVG file=/Users/florianm/Dropbox/Uni/Research/LiMiTS/trees/cariban_phylo/images/%s.svg REPLACE=yes;
    QUIT;
    end;
    """ % (filename, filename.replace("_distances.nex", ""), filename.replace("_distances.nex", ""))

    f = open("splitstree_cmd_temp.txt", "w")
    f.write(splitstree_cmd)
    f.close()

    bashCommand = "/Applications/SplitsTree/SplitsTree.app/Contents/MacOS/JavaApplicationStub -g -c /Users/florianm/Dropbox/Uni/Research/LiMiTS/trees/cariban_phylo/splitstree_cmd_temp.txt"
    process = subprocess.Popen(bashCommand.split())
    output, error = process.communicate()

    os.remove("splitstree_cmd_temp.txt")