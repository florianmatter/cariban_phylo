# This script create a distance matrix from a given CSV feature file
from lingpy import *
import csv
import os
import sys
from lingpy.convert.strings import write_nexus

#If no tree name is specified, fall back on swadesh list with own cognacy judgments. Else, use the specified filename
tree_name = "own_swadesh"
if len(sys.argv) > 1:
    tree_name = sys.argv[1].replace(".csv", "")

#Read the specified file and save it in lingpy format
reader = csv.DictReader(open("./phylo_files/%s.csv" % tree_name))
out = [["ID","DOCULECT","CONCEPT","COGID"]]
for i, row in enumerate(reader):
    lg_id = row["Language_ID"]
    out.append([
        i+1,
        lg_id,
        row["Feature_ID"],
        row["Value"]
    ])
    
#Read the lingpy temp file and create a wordlist
with open("lingpy_temp.tsv","w") as f:
    writer = csv.writer(f,delimiter="\t")
    for row in out:
        writer.writerow(row)
lp_list = Wordlist("lingpy_temp.tsv")
# os.remove("lingpy_temp.tsv")

# #Calculate a tree from the wordlist and save it
# lp_list.calculate("tree")
# # print(lp_list.tree.asciiArt())
# f = open("./newick_trees/%s.newick" % tree_name, "w")
# f.write(str(lp_list.tree))
# f.close()

print(lp_list)
#Calculate distances from the wordlist and create a nexus file
distances = lp_list.get_distances()
language_list = lp_list.cols
distance_matrix = dict(zip(language_list, distances))
nexus_out = write_nexus(lp_list, mode="splitstree", filename=None)
nexus_out += """
BEGIN Taxa;
DIMENSIONS ntax=%s;
TAXLABELS
""" % (len(language_list))
for i, lg in enumerate(language_list):
    nexus_out += "[%s] '%s'\n" % (i+1, lg)
nexus_out += """;
END; [Taxa]"""
f = open("./distance_matrices/%s_distances.nex" % tree_name, "w")
f.write(nexus_out)
f.close()