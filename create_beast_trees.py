#This script goes through every csv file in the phylo_files folder and uses beastling + beast to create a phylogenetic tree.
import subprocess
import os

phylo_dir = "./phylo_files/"
for filename in os.listdir(phylo_dir):
    if ".csv" not in filename: continue
    tree_name = filename.split(".")[0]
    beastling_conf = """[model %s]
    model=covarion
    data=phylo_files/%s
    rate_variation=True
    # [geography]
    # data=phylo_geo.csv
    [clock default]
    type=relaxed
    [MCMC]
    chainlength=2000000
    [admin]
    basename=%s
    log_params=True""" % (tree_name, filename, tree_name)
    f = open("cariban_phylo_tree_temp.conf", "w")
    f.write(beastling_conf)
    f.close()
    
    cmds = ["beastling --overwrite cariban_phylo_tree_temp.conf",
    "beast -overwrite %s.xml" % tree_name,
    ]
    
    for cmd in cmds:
        process = subprocess.Popen(cmd.split())
        output, error = process.communicate()
    
    os.remove("cariban_phylo_tree_temp.conf")