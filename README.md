## A phylogenetic classification of Cariban languages

This repo contains data and scripts used to create a phylogenetic classification of [Cariban](https://glottolog.org/resource/languoid/id/cari1283) languages, as part of [my PhD dissertation](https://florianmatter.gitlab.io/cariban-verbal-morphology/).
The basis of the classification are 100-word Swadesh lists (browsable [here](http://cariban.clld.org/morphemes#swadesh)).
Based on these lexical data, we get the following clustering:

![A NeighborNet using Swadesh lists](images/lex.svg)

The tight groupings contain obviously closely related languages: Mapoyo and Yawarana, Ikpeng and Arara, Akuriyó and Tiriyó, Hixkaryána and Waiwai, as well as the Pemongan group.
Some of the looser groupings have also been postulated in the literature: Taranoan (Meira [1998](#references)) with Carijona, Parukotoan with Werikyana (Gildea [1998](#references)), the Guianan branch spanning from Kari'ña to Wayaya (Gildea [2012](#references)), and the Pekodian languages with Bakairi (Meira and Franchetto [2005](#references)).
The Venezuelan branch (Gildea ([2003](#references)), Gildea ([2015](#references))) is only weakly supported, although the languages are next to each other (from Mapoyo to Ye'kwana).

The tree model based on the same lexical data looks like this:

![A phylogenetic tree based on lexical data](images/lex_tree_raw.png)

This tree has a group including all languages spoken in an area roughly between the Orinoco and the Amazon river, i.e. all languages north of the Amazon with the exception of Yukpa.
However, many branches are only weakly supported.
If one uses a conservative cutoff point of a posterior probability of 0.8 (Michael et al. [2015](#references): 14) to remove weakly supported nodes, the following tree emerges:

![A phylogenetic tree based on lexical data, pruned and colored](images/lex_80_tree_colored.png)

This tree is much less “lumping” than the previous one.
It contains (versions of) several branches postulated in the literature.
However, it groups Apalaí with Taranoan, with the exclusion of other languages of the Guianan branch, and no connection between any of the potential members of the Venezuelan branch is found.
What happens if we add morphological data, i.e. the etymologies of the Set I person markers?
We get the following NeighborNet and corresponding tree:

![A NeighborNet using Swadesh lists and Set I](images/lex_set1.svg)

![A phylogenetic tree based on lexical data and Set I](images/lex_set1_tree_raw.png)

Here, we see the Pemon-Panare grouping suggested by Gildea ([2003](#references)), Gildea ([2015](#references)).
It also shows a Guianan branch, with Parukotoan as a sister.

![A phylogenetic tree based on lexical data and Set I, pruned and colored](images/lex_set1_80_tree_colored.png)

<!-- bib:/Users/florianm/Dropbox/Uni/Research/LiMiTS/tools/cariban_cldf/raw/cariban_resolved.bib
bib:/Users/florianm/Dropbox/Uni/Research/LiMiTS/global_latex_files/non_cariban_resolved.bib-->

<!-- Different data sources are used and combined:
1. 100-word Swadesh lists (browsable [here](http://cariban.clld.org/morphemes#swadesh))
2. The so-called Set I person marking prefixes ([raw file](feature_files/setone.csv), Set I constructions are [here](http://cariban.clld.org/declarativetypes/decl))
2. the presence and morphosyntactic behavior of *\*t*-adding verbs (browsable [here](http://cariban.clld.org/morphemes#tadding))
3. several classification features appearing in the literature:
	* [Parukotoan branch](feature_files/parukotoan_features.csv) (Gildea ([1998](#references)), Meira et al. ([2010](#references)))
	* [Southern languages](feature_files/southern_features.csv) (Meira and Franchetto [2005](#references))
	* [Tiriyó & Akuriyó](feature_files/tiriyoan_features.csv) (Meira [1998](#references): 85)
	* [Venezuelan branch](feature_files/venezuelan_features.csv) (Gildea ([2003](#references)), Gildea ([2015](#references))) -->

# References
Gildea, Spike. 1998. On reconstructing grammar: Comparative Cariban morphosyntax. Oxford: Oxford University Press.  
Gildea, Spike. 2003. Proposing a new branch for the Cariban language family. Amerindia. 7–32.  
Gildea, Spike. 2012. Linguistic studies in the Cariban family. In Campbell, Lyle and Grondona, Verónica (eds.), 441–494. Berlin/New York: Mouton de Gruyter.  
Gildea, Spike. 2015. 15 years later: Current Perspectives on the Venezuelan Branch Hypothesis. (Paper presented at the Working Conference on Venezuelan Cariban Languages).  
Meira, Sérgio and Franchetto, Bruna. 2005. The Southern Cariban languages and the Cariban family. International Journal of American Linguistics 71. 127–192.  
Meira, Sérgio and Gildea, Spike and Hoff, Berend J. 2010. On the Origin of Ablaut in the Cariban Family. International Journal of American Linguistics 76. 477–515.  
Meira, Sérgio. 1998. A reconstruction of Proto-Taranoan: Phonology and Inflectional Morphology. (MA thesis, Rice University).  
Michael, Lev and Chousou-Polydouri, Natalia and Bartolomei, Keith and Donnelly, Erin and Meira, Sérgio and Wauters, Vivian and O'hagan, Zachary. 2015. A Bayesian phylogenetic classification of Tupí-Guaraní. LIAMES: Línguas Indígenas Americanas 15. 193–221.