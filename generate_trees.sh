#This script
# 1. generates an automated cognacy analysis using lingpy
# 2. combines existing feature lists into one large feature list
# 3. uses lingpy to generate distance matrices
# 4. creates NeighborNet networks from these matrices
# 5. does postprocessing of all generated files

python3 lingpy_swadesh_cognacy.py

rm phylo_files/*
python3 combine_features.py

rm newick_trees/*
rm distance_matrices/*
for file in ./phylo_files/*; do
	python3 lingpy_distances.py ${file##*/}
done

python3 create_networks.py

python3 create_beast_trees.py
for file in ./*; do
	if [[ $file =~ ".nex" ]]
	then
		echo "${file##*/}"
		name=${file%.nex}
		phyltr cat --burnin 25 $file | phyltr consensus > newick_trees/${name##*/}.newick
		phyltr cat --burnin 25 $file | phyltr consensus --frequency 0.8 > newick_trees/${name##*/}_80.newick
	fi
done

python3 ignore/draw_generated_trees.py
python3 pretty_trees.py
python3 deratify_newick.py

rm ./*.log
rm ./*.xml
rm ./*.state
rm ./*.nex

cd ..
python3 latexify_networks.py
python3 latexify_trees.py
cd cariban_phylo