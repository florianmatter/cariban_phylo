#This script combines lists of features to larger files which can then be used for beastling + beast
import csv
#Some files use glottocodes, some my internal IDs. This allows for quick conversion
glotto_dict = {}
id_dict = {}
#Collapse dialects into languages (kui is uxc, ing is kap etc.)
dialects = {}

cldf_dir = "/Users/florianm/Dropbox/Uni/Research/LiMiTS/tools/cariban_cldf/raw/"

with open(cldf_dir + "cariban_language_list.csv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=",")
    for row in csv_reader:
        glotto_dict[row["Glottocode"]] = row["ID"]
        id_dict[row["ID"]] = row["Glottocode"]
        if row["Dialect_Of"] != "":
            dialects[row["ID"]] = row["Dialect_Of"]

# Let's ignore Tamanaku for now…
ignore = ["tam"]
# Or let's include it
# ignore = []

#Create feature files for t-adding verbs and my own swadesh cognacy judgments
t_verbs_output = []
with open(cldf_dir + "cariban_t_verbs.csv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=",")
    for row in csv_reader:
        lang_id = glotto_dict[row["Language_ID"]]
        if lang_id in dialects.keys(): lang_id = dialects[lang_id]
        t_cognate_set = "t_" + row["Cognateset_ID"]
        cog_key = t_cognate_set + ":" + row["t?"]
        t_verbs_output.append([
            lang_id,
            t_cognate_set,
            row["t?"],
        ])
with open("./feature_files/t_verbs.csv", 'w+', newline='') as myfile:
     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
     wr.writerow(["Language_ID", "Feature_ID", "Value", "Form"])
     for entry in t_verbs_output:
         wr.writerow(entry)

own_swadesh_out = []
ignore_concepts = [9, 10, 15, 88, 89, 91, 95, 98]
ignore_concepts = [9, 14, 15, 88, 89, 98, 99]
# ignore_concepts = []
with open(cldf_dir + "cariban_swadesh_list.csv") as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=",")
    for row in csv_reader:
        lg = row["Language_ID"] 
        if lg in dialects.keys():
            lg = dialects[lg]
        # if lg not in ignore and int(row["Swadesh_Nr"]) not in ignore_concepts:
        if lg not in ignore:# and int(row["Swadesh_Nr"]) <= 100:
            if [lg, row["Feature_ID"], row["Cognateset_ID"], row["Value"]] not in own_swadesh_out:
                own_swadesh_out.append([lg, row["Feature_ID"], row["Cognateset_ID"], row["Value"]])
with open("./feature_files/own_swadesh.csv", 'w+', newline='') as myfile:
     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
     wr.writerow(["Language_ID", "Feature_ID", "Value", "Form"])
     for entry in own_swadesh_out:
         wr.writerow(entry)

#Create a file from which trees can be calculated
def generate_phylo_file(feature_files, name):
    out = []
    values = {}
    i = 1
    for filename in feature_files:
        with open("./feature_files/"+filename+".csv") as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=",")
            for row in csv_reader:
                #Ignore unknown feature values
                if row["Value"] == "?": continue
                lg = row["Language_ID"]
                if lg in dialects.keys():
                    lg = dialects[lg]
                cog_key = row["Feature_ID"] + ":" + row["Value"]
                if cog_key in values:
                    value = values[cog_key]
                else:
                    value = i
                    values[cog_key] = i
                    i += 1
                out.append([lg, row["Feature_ID"], value])
    with open("./phylo_files/"+name+".csv", 'w+', newline='') as myfile:
         wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
         wr.writerow(["Language_ID", "Feature_ID", "Value", "Form"])
         for entry in out:
             wr.writerow(entry)

# generate_phylo_file(["lingpy_swadesh", "setone"], "lingpy+setone")
generate_phylo_file(["own_swadesh", "setone"], "lex_set1")#just take everything
generate_phylo_file(["own_swadesh"], "lex")
# generate_phylo_file(["setone"], "set1")
# generate_phylo_file(["own_swadesh", "setone", "t_verbs"], "all_criteria")
# generate_phylo_file(["tiriyoan_features", "venezuelan_features", "parukotoan_features", "southern_features"], "traditional_criteria")

tree_sources = [
    # "own_swadesh",
    "lingpy_swadesh",
    # "setone",
    # "t_verbs",
    # "southern_features",
    # "tiriyoan_features",
    # "venezuelan_features",
    # "parukotoan_features"
]

for tree_name in tree_sources:
    generate_phylo_file([tree_name], tree_name)
    