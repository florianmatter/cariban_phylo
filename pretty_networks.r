library(phangorn)
# library(rgl)
library(magrittr) 
cons_tree = read.tree("newick_trees/lex.newick")
nnet = read.nexus.networx("distance_matrices/lex_distances-2.nex")
par = "#9900ff"
tar = "#ff3300"
gui = "#ff9933"
pek = "#00cc00"
pem = "#0033cc"
pempan = "#33ccff"
ven = "#0099cc"
color_hash = data.frame(
	"Akuriyo" = tar,
	"Apalai" = gui,
	"Wayana" = gui,
	"Waiwai" = par,
	"Hixkaryana" = par,
	"Werikyana" = par,
	"Yekwana" = ven,
	"Panare" = pempan,
	"Kapon" = pem, 
	"Pemon" = pem,
	"Macushi" = pem,
	"Yawarana" = ven,
	"Mapoyo" = ven,
	"Upper_Xingu_Carib" = "black",
	"Ikpeng" = pek,
	"Arara" = pek,
	"Bakairi" = pek,
	"Yukpa" = "black",
	"Waimiri-Atroari" = "black",
	"Karina" = gui,
	"Carijona" = tar,
	"Tiriyo" = tar
)

edge_colors = rep("black", 388)
tips = c("aku", "apa", "way", "wai", "hix", "kax", "mak", "pan", "kap", "pem", "mac", "yab", "map", "uxc", "ikp", "ara", "bak", "yuk", "wmr", "kar", "car", "tri")
names(color_hash) = tips

for (tip in tips){
	target_node = which(nnet$translate$label == tip)
	target_edges = which(nnet$edge[,2] == target_node)
	for (target_edge in target_edges){
		print(target_edge)
		edge_colors[target_edge] = toString(color_hash[[tip]])
	}
}

attributes(nnet)
nnet$edge
# plot(
# 	nnet,
# 	"2D",
# 	tip.color=as.character(as.vector(color_hash[1,])),
# 	cex=.6,
# 	edge.width = 1,
# 	# show.node.label = TRUE,
# 	show.tip.label = TRUE,
# 	show.nodes = TRUE,
# 	# show.edge.label = TRUE,
# 	show.splits = TRUE,
# 	show.split.label = TRUE,
# 	edge.color = edge_colors
# )